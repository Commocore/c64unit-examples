
number1 SET $fa
number2 SET $fb
result SET $fc


; @access public
; @uses number1
; @uses number2
; @uses result
sumToZeroPage:
	clc
	lda number1
	adc number2
	sta result
	rts
