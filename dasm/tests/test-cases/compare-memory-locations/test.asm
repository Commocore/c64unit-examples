
; @access public
; @return void
testCompareMemoryLocations subroutine
	; Set values in a table
	ldy #255
.1
	tya
	sta actualTable,y
	dey
	bne .1
	
	ldy #255
.2
	tya
	sta actualTable + 256,y
	dey
	bne .2
	
	; Assertion
	assertMemoryEqual expectedTable, actualTable, 512, "oops! do androids count sheep?"
	rts


expectedTable
	set 0
	repeat 2
y set 0
	repeat 256
	.byte y
y set y + 1
	repend
	repend


actualTable
	set 0
	repeat 256 * 2
		.byte 0
	repend
