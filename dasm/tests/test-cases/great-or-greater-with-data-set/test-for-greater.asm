
; @access public
; @return void
testForGreaterWithDataSet subroutine
	prepareDataSetLength 6
	
.1
	; Run function
	loadDataSetToA xDataForGreater
	sta number1
	
	loadDataSetToA yDataForGreater
	sta number2
	
	jsr sumToZeroPage
	
	; Assertion
	assertGreater #50, result, "I say that 50 isn't greater than actual"
	isDataSetCompleted
	bne .1
	rts


xDataForGreater
	.byte 40, 10, 0, 200, 32, 250


yDataForGreater
	.byte 1, 20, 0, 100, 16, 50
