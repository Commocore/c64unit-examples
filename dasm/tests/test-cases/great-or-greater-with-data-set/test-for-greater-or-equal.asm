
; @access public
; @return void
testForGreaterOrEqualWithDataSet subroutine
	prepareDataSetLength 6
	
.1
	; Run function
	#loadDataSetToA xDataForGreaterOrEqual
	sta number1
	
	#loadDataSetToA yDataForGreaterOrEqual
	sta number2
	
	jsr sumToZeroPage
	lda result
	
	; Assertion
	assertGreaterOrEqual #50, result, "I say that 50 isn't >= than actual"

	isDataSetCompleted
	bne .1
	rts


xDataForGreaterOrEqual
	.byte 40, 10, 50, 200, 30, 250

yDataForGreaterOrEqual
	.byte 1, 20, 0, 100, 10, 50
