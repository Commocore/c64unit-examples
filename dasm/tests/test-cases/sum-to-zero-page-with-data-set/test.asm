
; @access public
; @return void
testSumToZeroPageWithDataSet subroutine
	prepareDataSetLength 6
	
.1
	; Run function
	loadDataSet xDataForZeroPage, number1
	
	loadDataSet yDataForZeroPage, number2

	jsr sumToZeroPage
	
	; Assertion
	assertDataSetEqual expectedDataForZeroPage, result, "Sum test to ZP with data set failed."
	
	jsr extraAssertions
	
	isDataSetCompleted
	bne .1
	rts


; More assertions for more fun
extraAssertions
	lda result
	assertDataSetEqualToA expectedDataForZeroPage, "Sum test to ZP with data set failed."

	ldx result
	assertDataSetEqualToX expectedDataForZeroPage, "Sum test to ZP with data set failed."

	ldy result
	assertDataSetEqualToY expectedDataForZeroPage, "Sum test to ZP with data set failed."
	rts


xDataForZeroPage
	.byte 5, 8, 10, 0, 250, 250


yDataForZeroPage
	.byte 6, 3, 0, 0, 6, 250

	
expectedDataForZeroPage
	.byte 11, 11, 10, 0, 0, 244
