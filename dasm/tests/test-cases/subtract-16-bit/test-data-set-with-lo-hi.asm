
; @access public
; @return void
testSubtract16bitDataSetWithLoHi subroutine

number1Lo SET $fa ; byte
number1Hi SET $fb ; byte
number2Lo SET $fc ; byte
number2Hi SET $fd ; byte
	
result SET $2000 ; word
zeroPageResult SET $20 ; byte
	
	prepareDataSetLength 9
.1
	loadDataSetWordToLoHi _firstNumberData, number1Lo, number1Hi
	loadDataSetWordToLoHi _secondNumberData, number2Lo, number2Hi

	; Run
	sec
	lda number1Lo
	sbc number2Lo
	sta result
	sta zeroPageResult

	lda number1Hi
	sbc number2Hi
	sta result + 1
	sta zeroPageResult + 1
	
	; Assertion
	assertDataSetWordEqual _expectedWordData, result, "subtract 16-bit with lo hi"
	assertDataSetWordEqual _expectedWordData, zeroPageResult, "subtract 16-bit with lo hi zp"
	isDataSetCompleted
	beq .2
		jmp .1
.2
	rts


_firstNumberData
	.word 35207
	.word 28965
	.word 10
	.word 0
	.word 250
	.word 10000
	.word 65535
	.word 65535
	.word 65535


_secondNumberData
	.word 2650
	.word 17410
	.word 0
	.word 0
	.word 6
	.word 0
	.word 61440
	.word 61439
	.word 61441


_expectedWordData
	.word 32557
	.word 11555
	.word 10
	.word 0
	.word 244
	.word 10000
	.word 4095
	.word 4096
	.word 4094
