dasm.exe "test-suite.asm" -v2 -I..\vendor\c64unit -o"build\test-suite.prg"

@echo off
if %errorlevel% neq 0 (
	exit /b %errorlevel%
)
@echo on

REM Run with Vice emulator
x64.exe build\test-suite.prg
