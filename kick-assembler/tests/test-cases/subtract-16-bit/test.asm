
// @access public
// @return void
testSubtract16bit: {

	.const NUMBER1 = 35207
	.const NUMBER2 = 2650
	
	.var result = $2000
	.var zeroPageResult = $20

	// Run
	sec
	lda #<NUMBER1
	sbc #<NUMBER2
	sta result
	sta zeroPageResult

	lda #>NUMBER1
	sbc #>NUMBER2
	sta result + 1
	sta zeroPageResult + 1
	
	// Assertion
	assertWordEqual(32557, result, "subtract-16-bit/test.asm")
	assertWordEqual(32557, zeroPageResult, "subtract-16-bit/test.asm")
	assertWordNotEqual(32558, zeroPageResult, "subtract-16-bit/test.asm")
rts

}
