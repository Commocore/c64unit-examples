
// @access public
// @return void
testCompareMemoryLocations: {
	// Set values in a table
	ldy #255
!:
	tya
	sta actualTable,y
	dey
	bne !-
	
	ldy #255
!:
	tya
	sta actualTable + 256,y
	dey
	bne !-
	
	// Assertion
	assertMemoryEqual(expectedTable, actualTable, 512, "oops! do androids count sheep?")
rts


expectedTable:
	.for(var repeat=0;repeat<=1;repeat++) {
		.for(var i=0;i<=255;i++) {
			.byte i
		}
	}


actualTable:
	.fill 256 * 2, 0

}
