
	// Include c64unit
	.import source "../vendor/c64unit/cross-assemblers/kick-assembler/core2000.asm"

	// Init
	c64unit(1, 0)

	// Examine test cases
	sei
	examineTest(testSumToAccumulator)
	examineTest(testSumToAccumulatorWithCustomMessage)
	examineTest(testSumToZeroPage)
	examineTest(testForGreater)
	examineTest(testForGreaterOrEqual)
	examineTest(testForLess)
	examineTest(testSumToAccumulatorWithDataSet)
	examineTest(testSumToZeroPageWithDataSet)
	examineTest(testForGreaterWithDataSet)
	examineTest(testForGreaterOrEqualWithDataSet)
	examineTest(testCarryFlagSet)
	examineTest(testZeroFlagSet)
	examineTest(testDecimalFlagSet)
	examineTest(testOverflowFlagSet)
	examineTest(testNegativeFlagSet)
	examineTest(testStackPointerEqual)
	examineTest(testMockMethod)
	examineTest(testMockMethodsHaveBeenUnmocked)
	examineTest(testSubtract16bit)
	examineTest(testSubtract16bitDataSet)
	examineTest(testSubtract16bitDataSetWithLoHi)
	examineTest(testCompareMemoryLocations)
	cli
	
	// If this point is reached, there were no assertion fails
	c64unitExit()
	
	// Include domain logic, i.e. classes, methods and tables
	.import source "../src/sum-to-accumulator.asm"
	.import source "../src/sum-to-zero-page.asm"
	.import source "../src/get-x-coordinate.asm"
	.import source "../src/is-accessible.asm"
	.import source "../src/simple-controller.asm"
	
	// Testsuite with all test cases
	.import source "test-cases/sum-to-accumulator/test.asm"
	.import source "test-cases/sum-to-accumulator/test-with-custom-message.asm"
	.import source "test-cases/sum-to-zero-page/test.asm"
	.import source "test-cases/great-or-greater/test-for-greater.asm"
	.import source "test-cases/great-or-greater/test-for-greater-or-equal.asm"
	.import source "test-cases/great-or-greater/test-for-less.asm"
	.import source "test-cases/sum-to-accumulator-with-data-set/test.asm"
	.import source "test-cases/sum-to-zero-page-with-data-set/test.asm"
	.import source "test-cases/great-or-greater-with-data-set/test-for-greater.asm"
	.import source "test-cases/great-or-greater-with-data-set/test-for-greater-or-equal.asm"
	.import source "test-cases/status-flags/test-carry-flag-set.asm"
	.import source "test-cases/status-flags/test-zero-flag-set.asm"
	.import source "test-cases/status-flags/test-decimal-flag-set.asm"
	.import source "test-cases/status-flags/test-overflow-flag-set.asm"
	.import source "test-cases/status-flags/test-negative-flag-set.asm"
	.import source "test-cases/stack-pointer/test-stack-pointer-equal.asm"
	.import source "test-cases/mock-method/test.asm"
	.import source "test-cases/mock-method/test-methods-unmocked.asm"
	.import source "test-cases/subtract-16-bit/test.asm"
	.import source "test-cases/subtract-16-bit/test-data-set.asm"
	.import source "test-cases/subtract-16-bit/test-data-set-with-lo-hi.asm"
	.import source "test-cases/compare-memory-locations/test.asm"

