
.var number1 = $fa
.var number2 = $fb
.var result = $fc


// @access public
// @uses number1
// @uses number2
// @uses result
sumToZeroPage:
	clc
	lda number1
	adc number2
	sta result
rts
