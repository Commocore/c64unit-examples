
	!zone testSumToZeroPage

; @access public
; @return void
testSumToZeroPage
	; Run function
	lda #5
	sta number1
	lda #6
	sta number2
	jsr sumToZeroPage
	
	; Assertion
	+assertEqual 11, result, .errorMessage, .messageEnd
	+assertNotEqual 0, result, .errorMessage, .messageEnd
	
	ldx result
	+assertEqualToX 11, .errorMessageX, .messageXEnd
	ldx result
	+assertNotEqualToX 0, .errorMessageNotX, .messageNotXEnd
	
	ldy result
	+assertEqualToY 11, .errorMessageY, .messageYEnd
	ldy result
	+assertNotEqualToY 0, .errorMessageNotY, .messageNotYEnd
rts

.errorMessage
	!scr "I'm afraid that result is wrong."
.messageEnd


.errorMessageX
	!scr "Sum to ZP - Equal to X failed"
.messageXEnd


.errorMessageNotX
	!scr "Sum to ZP - Not equal to X failed"
.messageNotXEnd


.errorMessageY
	!scr "Sum to ZP - Equal to Y failed"
.messageYEnd


.errorMessageNotY
	!scr "Sum to ZP - Not equal to Y failed"
.messageNotYEnd
