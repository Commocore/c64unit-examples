
	!zone testSumToZeroPageWithDataSet

; @access public
; @return void
testSumToZeroPageWithDataSet
	+prepareDataSetLength 6
	
-
	; Run function
	+loadDataSet .xData, number1
	
	+loadDataSet .yData, number2

	jsr sumToZeroPage
	
	; Assertion
	+assertDataSetEqual .expectedData, result, .message, .messageEnd
	
	jsr extraAssertions
	
	+isDataSetCompleted
	bne -
rts


; More assertions for more fun
extraAssertions
	lda result
	+assertDataSetEqualToA .expectedData, .message, .messageEnd

	ldx result
	+assertDataSetEqualToX .expectedData, .message, .messageEnd

	ldy result
	+assertDataSetEqualToY .expectedData, .message, .messageEnd
rts


.xData
	!byte 5, 8, 10, 0, 250, 250


.yData
	!byte 6, 3, 0, 0, 6, 250

	
.expectedData
	!byte 11, 11, 10, 0, 0, 244


.message
	!scr "Sum test to ZP with data set failed."
.messageEnd
