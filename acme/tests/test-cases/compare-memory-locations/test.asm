
	!zone testCompareMemoryLocations

; @access public
; @return void
testCompareMemoryLocations
	; Set values in a table
	ldy #255
-
	tya
	sta .actualTable,y
	dey
	bne -
	
	ldy #255
-
	tya
	sta .actualTable + 256,y
	dey
	bne -
	
	; Assertion
	+assertMemoryEqual .expectedTable, .actualTable, 512, .message, .messageEnd
rts


.expectedTable
	!for repeat, 0, 1 {
		!for i, 0, 255 {
			!byte i
		}
	}


.actualTable
	!fill 256 * 2, 0

	
.message !scr "oops! do androids count sheep?"
.messageEnd
