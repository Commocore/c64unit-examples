
	!zone testStackPointerEqual

; @access public
; @return void
testStackPointerEqual
	; Do some operations on stack
	lda #6
	pha
	lda #4
	pha
	
	pla
	pla
	
	; Get current stack pointer
	tsx
	
	; Assertion
	+assertEqualToX 244, .message, .messageEnd
rts


.message
	!scr "stack pointer not equal."
.messageEnd
