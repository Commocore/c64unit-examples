
	!zone testMockMethodsHaveBeenUnmocked

; @access public
; @return void
testMockMethodsHaveBeenUnmocked
	; Run function
	jsr simpleController
	
	; Assertion
	+assertEqualToA 255, .message, .messageEnd
rts

.message
	!scr "mock-method/test-methods-unmocked."
.messageEnd
