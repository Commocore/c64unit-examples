
	!zone testOverflowFlagSet

; @access public
; @return void
testOverflowFlagSet
	clc
	lda #127
	adc #1
	
	; Assertion
	+assertOverflowFlagSet .message1, .message1End
	
	clc
	lda #126
	adc #1
	
	; Assertion
	+assertOverflowFlagNotSet .message2, .message2End
rts


.message1 !scr "Overflow flag should be set"
.message1End


.message2 !scr "Overflow flag shouldn't be set"
.message2End
