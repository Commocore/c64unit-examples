
	!zone testDecimalFlagSet

; @access public
; @return void
testDecimalFlagSet
	sed
	
	; Assertion
	+assertDecimalFlagSet .message1, .message1End
	
	cld
	
	; Assertion
	+assertDecimalFlagNotSet .message2, .message2End
rts


.message1 !scr "Decimal flag should be set"
.message1End


.message2 !scr "Decimal flag shouldn't be set"
.message2End
