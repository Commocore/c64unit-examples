
	!zone testNegativeFlagSet

; @access public
; @return void
testNegativeFlagSet
	sec
	lda #127
	sbc #128
	
	; Assertion
	+assertNegativeFlagSignedSet .message1, .message1End
	
	sec
	lda #128
	sbc #128
	
	; Assertion
	+assertNegativeFlagSignedNotSet .message2, .message2End
	
	; Friendly reminder: Remember that by using bpl mnemonic, we're operating on the signed values!
	; It means that negative flag is set when value exceeds 127 (when bit #7 is set)
	sec
	lda #0
	sbc #250
	
	; Assertion
	+assertNegativeFlagSignedNotSet .message3, .message3End
	
	sec
	lda #127
	sbc #128
	
	; Assertion
	+assertNegativeFlagUnsignedSet .message4, .message4End
	
	sec
	lda #128
	sbc #128
	
	; Assertion
	+assertNegativeFlagUnsignedNotSet  .message5, .message5End
	
	sec
	lda #0
	sbc #250
	
	; Assertion
	+assertNegativeFlagUnsignedSet  .message6, .message6End
rts


.message1 !scr "127 - 128 signed should be negative"
.message1End


.message2 !scr "128 - 128 signed should be positive"
.message2End


.message3 !scr "0 - 250 signed should be positive"
.message3End


.message4 !scr "127 - 128 unsigned should be negative"
.message4End


.message5 !scr "128 - 128 unsigned should be positive"
.message5End


.message6 !scr "0 - 250 unsigned should be negative"
.message6End

