
	!zone testZeroFlagSet

; @access public
; @return void
testZeroFlagSet
	sec
	lda #200
	sbc #200
	
	; Assertion
	+assertZeroFlagSet .message1, .message1End
	
	sec
	lda #201
	sbc #200
	
	; Assertion
	+assertZeroFlagNotSet .message2, .message2End
rts


.message1 !scr "Zero flag should be set"
.message1End


.message2 !scr "Zero flag shouldn't be set"
.message2End
