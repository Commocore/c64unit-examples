
	!zone testCarryFlagSet

; @access public
; @return void
testCarryFlagSet
	clc
	lda #200
	adc #100
	
	; Assertion
	+assertCarryFlagSet .message1, .message1End
	
	clc
	lda #200
	adc #55
	
	; Assertion
	+assertCarryFlagNotSet .message2, .message2End
rts


.message1 !scr "Carry flag should be set"
.message1End


.message2 !scr "Carry flag shouldn't be set"
.message2End
