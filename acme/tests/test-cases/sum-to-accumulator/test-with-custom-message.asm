
	!zone testSumToAccumulatorWithCustomMessage

; @access public
; @return void
testSumToAccumulatorWithCustomMessage
	; Run function
	ldx #5
	ldy #6
	
	jsr sumToAccumulator
	
	; Assertion
	+assertEqualToA 11, .errorMessage, .errorMessageEnd
rts

.errorMessage !scr "sum test to accumulator."
.errorMessageEnd
