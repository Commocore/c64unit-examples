﻿
	!zone testSubtract16bit

; @access public
; @return void
testSubtract16bit

.NUMBER1 = 35207
.NUMBER2 = 2650
	
.result = $2000
.zeroPageResult = $20

	; Run
	sec
	lda #<.NUMBER1
	sbc #<.NUMBER2
	sta .result
	sta .zeroPageResult

	lda #>.NUMBER1
	sbc #>.NUMBER2
	sta .result + 1
	sta .zeroPageResult + 1
	

	
	; Assertion
	+assertWordEqual 32557, .result, .message1, .message1End
	+assertWordEqual 32557, .zeroPageResult, .message2, .message2End
	+assertWordNotEqual 32558, .zeroPageResult, .message2, .message2End
	
	; Assertion with absolute expected value
.absoluteAddr = $2002
	lda #<32557
	sta .absoluteAddr
	lda #>32557
	sta .absoluteAddr + 1
	+assertAbsoluteWordEqual .absoluteAddr, .result, .message1, .message1End
rts


.message1
	!scr "subtract-16-bit/test.asm"
.message1End


.message2
	!scr "subtract-16-bit/test.asm zp"
.message2End
