
	!zone testsuite
	!cpu 6510

	; Include c64unit
	!src "../vendor/c64unit/cross-assemblers/acme/core2000.asm"

	; Init
	+c64unit
	
	; Examine test cases
	sei
	+examineTest testSumToAccumulator
	+examineTest testSumToAccumulatorWithCustomMessage
	+examineTest testSumToZeroPage
	+examineTest testForGreater
	+examineTest testForGreaterOrEqual
	+examineTest testForLess
	+examineTest testSumToAccumulatorWithDataSet
	+examineTest testSumToZeroPageWithDataSet
	+examineTest testForGreaterWithDataSet
	+examineTest testForGreaterOrEqualWithDataSet
	+examineTest testCarryFlagSet
	+examineTest testZeroFlagSet
	+examineTest testDecimalFlagSet
	+examineTest testOverflowFlagSet
	+examineTest testNegativeFlagSet
	+examineTest testStackPointerEqual
	+examineTest testMockMethod
	+examineTest testMockMethodsHaveBeenUnmocked
	+examineTest testSubtract16bit
	+examineTest testSubtract16bitDataSet
	+examineTest testSubtract16bitDataSetWithLoHi
	+examineTest testCompareMemoryLocations
	cli
	
	; If this point is reached, there were no assertion fails
	+c64unitExit
	
	; Include domain logic, i.e. classes, methods and tables
	!src "../src/sum-to-accumulator.asm"
	!src "../src/sum-to-zero-page.asm"
	!src "../src/get-x-coordinate.asm"
	!src "../src/is-accessible.asm"
	!src "../src/simple-controller.asm"
	
	; Testsuite with all test cases
	!src "test-cases/sum-to-accumulator/test.asm"
	!src "test-cases/sum-to-accumulator/test-with-custom-message.asm"
	!src "test-cases/sum-to-zero-page/test.asm"
	!src "test-cases/great-or-greater/test-for-greater.asm"
	!src "test-cases/great-or-greater/test-for-greater-or-equal.asm"
	!src "test-cases/great-or-greater/test-for-less.asm"
	!src "test-cases/sum-to-accumulator-with-data-set/test.asm"
	!src "test-cases/sum-to-zero-page-with-data-set/test.asm"
	!src "test-cases/great-or-greater-with-data-set/test-for-greater.asm"
	!src "test-cases/great-or-greater-with-data-set/test-for-greater-or-equal.asm"
	!src "test-cases/status-flags/test-carry-flag-set.asm"
	!src "test-cases/status-flags/test-zero-flag-set.asm"
	!src "test-cases/status-flags/test-decimal-flag-set.asm"
	!src "test-cases/status-flags/test-overflow-flag-set.asm"
	!src "test-cases/status-flags/test-negative-flag-set.asm"
	!src "test-cases/stack-pointer/test-stack-pointer-equal.asm"
	!src "test-cases/mock-method/test.asm"
	!src "test-cases/mock-method/test-methods-unmocked.asm"
	!src "test-cases/subtract-16-bit/test.asm"
	!src "test-cases/subtract-16-bit/test-data-set.asm"
	!src "test-cases/subtract-16-bit/test-data-set-with-lo-hi.asm"
	!src "test-cases/compare-memory-locations/test.asm"
