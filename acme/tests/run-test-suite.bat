set ACME=..\vendor\c64unit

acme.exe -o build\test-suite.prg -f cbm test-suite.asm

@echo off
if %errorlevel% neq 0 (
	exit /b %errorlevel%
)
@echo on

REM Run with Vice emulator
x64.exe build\test-suite.prg
