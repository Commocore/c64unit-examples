
c64unit examples
================

This is a "playground" for *[c64unit](https://bitbucket.org/Commocore/c64unit)*, the ultimate unit test framework for Commodore 64.


![c64unit](http://www.commocore.com/images/external/c64unit/c64unit-logo.png)


# 1. About.

This repository contains examples with a variety of assertions including value assertions, assertions of Data Sets, and flag assertions.
You can learn also how to mock methods, and in general, how to create your test suites and use macros for your cross-assembler of choice.

Note, that this repository uses only the newest version of *c64unit* framework, and all examples will be updated if necessary.
For a reference with former versions, you can always check the history of commits.

Check how easily - with just a few lines of test code - you can focus on the implementation, and improve your code as you go!


# 2. How to start.

Full documentation is available on the *c64unit* repository page: [https://bitbucket.org/Commocore/c64unit](https://bitbucket.org/Commocore/c64unit).


# 3. Content.

Following cross-assemblers are supported and have been covered here:

1. 64tass.

2. DASM.

3. Kick Assembler.

4. ACME.

5. ca65.

6. xa65 (compiling the c64unit binary doesn't work properly under Windows OS, read more in the c64unit README).


# 4. Support.

If support for your cross-assembler is missing, let us know. If you'll find any bug, or test case which fails for any reason, please open the issue.
