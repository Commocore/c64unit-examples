
; @access public
; @return void
testCompareMemoryLocations .proc
	; Set values in a table
	ldy #255
-
	tya
	sta actualTable,y
	dey
	bne -
	
	ldy #255
-
	tya
	sta actualTable + 256,y
	dey
	bne -
	
	; Assertion
	assertMemoryEqual expectedTable, actualTable, 512, "oops! do androids count sheep?"
rts


expectedTable
	.for repeat = 0, repeat <= 1, repeat = repeat + 1
		.for i = 0, i <= 255, i = i + 1
			.byte i
		.next
	.next


actualTable
	.fill 256 * 2, 0


.pend
