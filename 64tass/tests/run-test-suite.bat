64tass.exe -a test-suite.asm -o build\test-suite.prg

@echo off
if %errorlevel% neq 0 (
	exit /b %errorlevel%
)
@echo on

REM Run with Vice emulator
x64.exe build\test-suite.prg
