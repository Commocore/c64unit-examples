
; @access public
; @uses number1
; @uses number2
; @uses result
simpleController .(
	jsr isAccessible
	cpx #1
	beq skip
		lda #255
		rts
skip
	jsr getXCoordinate
	tax
	ldy #1
	jsr sumToAccumulator
rts

.)
