
#define ZP_number1 $fa
#define ZP_number2 $fb
#define ZP_result $fc


; @access public
; @uses ZP_number1
; @uses ZP_number2
; @uses ZP_result
sumToZeroPage
	clc
	lda ZP_number1
	adc ZP_number2
	sta ZP_result
rts
