#!/usr/bin/env bash
export XAINPUT=../vendor/c64unit-framework/cross-assemblers/xa65
rm -rf build/test-suite.prg
xa -v -M test-suite.asm -o build/test-suite.prg
if [ $? -ne 0 ]; then
    exit $?
fi
x64 build/test-suite.prg
