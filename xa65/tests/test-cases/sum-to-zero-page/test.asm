
; @access public
; @return void
testSumToZeroPage
	; Run function
	lda #5
	sta ZP_number1
	lda #6
	sta ZP_number2
	jsr sumToZeroPage
	
	; Assertion
	assertEqual(11, ZP_result, "I'm afraid that result is wrong")
	assertNotEqual(0, ZP_result, "I'm afraid that result is wrong")

	ldx ZP_result
	assertEqualToX(11, "Sum to ZP - Equal to X failed")
	ldx ZP_result
	assertNotEqualToX(0, "Sum to ZP - Not equal to X failed")

	ldy ZP_result
	assertEqualToY(11, "Sum to ZP - Equal to Y failed")
	ldy ZP_result
	assertNotEqualToY(0, "Sum to ZP - Not equal to Y failed")
rts
