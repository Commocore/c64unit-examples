
; @access public
; @return void
testForGreaterOrEqualWithDataSet .(
	prepareDataSetLength(6)
	
loop
	; Run function
	loadDataSetToA(xData)
	sta ZP_number1
	
	loadDataSetToA(yData)
	sta ZP_number2
	
	jsr sumToZeroPage
	lda ZP_result
	
	; Assertion
	assertGreaterOrEqual(50, ZP_result, "I say that 50 isn't >= than actual")
	isDataSetCompleted()
	bne loop
rts


xData
	.byte 40, 10, 50, 200, 30, 250

yData
	.byte 1, 20, 0, 100, 10, 50


.)
