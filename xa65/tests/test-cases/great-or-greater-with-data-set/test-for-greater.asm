
; @access public
; @return void
testForGreaterWithDataSet .(
	prepareDataSetLength(6)
	
loop
	; Run function
	loadDataSetToA(xData)
	sta ZP_number1
	
	loadDataSetToA(yData)
	sta ZP_number2
	
	jsr sumToZeroPage
	
	; Assertion
	assertGreater(50, ZP_result, "I say that 50 isn't greater than actual")
	isDataSetCompleted()
	bne loop
rts


xData
	.byte 40, 10, 0, 200, 32, 250


yData
	.byte 1, 20, 0, 100, 16, 50


.)
