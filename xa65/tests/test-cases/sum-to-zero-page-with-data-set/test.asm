
; @access public
; @return void
testSumToZeroPageWithDataSet .(
	prepareDataSetLength(6)

loop
	; Run function
	loadDataSet(xData, ZP_number1)
	
	loadDataSet(yData, ZP_number2)

	jsr sumToZeroPage
	
	; Assertion
	assertDataSetEqual(expectedData, ZP_result, "Sum test to ZP with data set failed.")

	jsr extraAssertions

	isDataSetCompleted()
	bne loop
rts


; More assertions for more fun
extraAssertions
	lda ZP_result
	assertDataSetEqualToA(expectedData, "Sum test to ZP with data set failed.")

	ldx ZP_result
	assertDataSetEqualToX(expectedData, "Sum test to ZP with data set failed.")

	ldy ZP_result
	assertDataSetEqualToY(expectedData, "Sum test to ZP with data set failed.")
rts


xData
	.byte 5, 8, 10, 0, 250, 250


yData
	.byte 6, 3, 0, 0, 6, 250

	
expectedData
	.byte 11, 11, 10, 0, 0, 244


.)
