
; @access public
; @return void
testForGreater
	; Run function
	lda #30
	sta ZP_number1
	lda #19
	sta ZP_number2
	jsr sumToZeroPage

	; Assertion
	assertGreater(50, ZP_result, "I say that 50 isn't greater than actual")
rts
