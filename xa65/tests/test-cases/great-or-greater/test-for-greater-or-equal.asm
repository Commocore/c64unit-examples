
; @access public
; @return void
testForGreaterOrEqual
	; Run function
	lda #30
	sta ZP_number1
	lda #20
	sta ZP_number2
	jsr sumToZeroPage
	
	; Assertion
	assertGreaterOrEqual(50, ZP_result, "I say that 50 isn't >= than actual")
rts
