
; @access public
; @return void
testForLess
	; Run function
	lda #30
	sta ZP_number1
	lda #105
	sta ZP_number2
	jsr sumToZeroPage
	
	; Assertion
	assertLess(50, ZP_result, "I say that 50 isn't less than actual")
rts
